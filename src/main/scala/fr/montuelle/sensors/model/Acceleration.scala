package fr.montuelle.sensors.model


case class Acceleration (
                        x: Float,
                        y: Float,
                        z: Float,
                        timestamp: Long

                          )